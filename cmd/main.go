package main

import (
	"fmt"

	repository "go-series/pkg/repositories"
	router "go-series/pkg/router"
	service "go-series/pkg/services"

	"github.com/gofiber/fiber/v2"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	var db *gorm.DB

	//step1: initial database instance
	conn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=%v sslmode=%v TimeZone=%v search_path=%v",
		"localhost", "postgres", "password", "postgres", "5432", "disable", "Asia/Bangkok", "golang_db",
	)

	var err error
	db, err = gorm.Open(postgres.Open(conn))
	if err != nil {
		panic(fmt.Errorf("error: %v", err))
	}

	//step2: regis repository layer
	repository := repository.NewCustomerRepository(db)

	//step3: regis service layer
	service := service.NewCustomerService(repository)

	//step4: regis handler
	app := fiber.New()
	router.RegisterRoutes(app, service)
	err = app.Listen(":9001")
	if err != nil {

	}

	//preveting error
	if service != nil {

	}
}
