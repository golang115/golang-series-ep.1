// welcome to my golang series

// details: we will learn about 
//     -> pattern design
//     -> dependency injection of golang
//     -> how to use gorm lib




#significant 
//  - 1. export GOPROXY="https://goproxy.io,direct"
//  - 2. you have to run  "go mod init [module-name]" ex. go mod init demo-1
//  - 3. Once you want to import your package, you have types "module-name" first the sub-folder name     
#================================

#series: golang + pattern design
// #let's start
//   1. creating db instance
//       |- run cmd: go mod init go-series
//       |- run cmd: go get -u gorm.io/gorm
//       |- creating package main

  2. creating repository layer
    //   |- repositories folder
    //   |- interface file
    //   |- implement file
    //   |- domain model like a database
    //   |- regis in main.go

  3. creating service layer
    //   |- services folder
    //   |- interface file
    //   |- implement file
    //   |- creating model request and response that you want
    //   |- regis in main.go

  4. creating presentation layer
    //   |- run: go get github.com/gofiber/fiber/v2
    //   |- creating api/v1/handler.go file
    //   |- creating router/router.go
    //   |- regis in main.go

=======================


# ==========================
#         associated
# mr.Siravich Thunmanuthum
# ==========================

thanks for watching 
        