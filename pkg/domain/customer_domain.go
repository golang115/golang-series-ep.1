package domain

// GolangCustomer --
type Customer struct {
	FirstName  string `db:"first_name"`
	LastName   string `db:"last_name"`
	Company    string `db:"company"`
	Address    string `db:"address"`
	City       string `db:"city"`
	State      string `db:"state"`
	Country    string `db:"country"`
	PostalCode string `db:"postal_code"`
	Phone      string `db:"phone"`
	FAX        string `db:"fax"`
	Email      string `db:"email"`
}
