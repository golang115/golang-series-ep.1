package repository

import (
	"go-series/pkg/domain"

	"gorm.io/gorm"
)

type CustomerRepository struct {
	db *gorm.DB
}

// init repo with gorm
func NewCustomerRepository(gormDB *gorm.DB) *CustomerRepository {
	return &CustomerRepository{db: gormDB}
}

// implement interface
func (repo CustomerRepository) GetAll() []domain.Customer {
	var res []domain.Customer
	repo.db.Debug().Find(&res) //.Debug() is show SELECT * FROM "customers"
	return res
}
