package repository

import "go-series/pkg/domain"

// creating interface
type ICustomerRepository interface {
	GetAll() []domain.Customer
}
