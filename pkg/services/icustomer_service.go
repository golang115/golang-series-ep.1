package service

import (
	model "go-series/pkg/model/response"
)

type ICustomerService interface {
	GetAll() *model.CustomerResponse
}
