package service

import (
	model "go-series/pkg/model/response"
	repository "go-series/pkg/repositories"
)

type CustermerService struct {
	customerRepository repository.ICustomerRepository
}

// inject repo to service layer
func NewCustomerService(repo repository.ICustomerRepository) *CustermerService {
	return &CustermerService{customerRepository: repo}
}

// implement interface service layer
func (repo CustermerService) GetAll() *model.CustomerResponse {
	res := new(model.CustomerResponse)
	data := repo.customerRepository.GetAll()

	for _, item := range data {
		res.Customers = append(res.Customers, model.CustomerModel{
			FirstName:  item.FirstName,
			LastName:   item.LastName,
			Company:    item.Company,
			Address:    item.Address,
			City:       item.City,
			State:      item.State,
			Country:    item.Country,
			PostalCode: item.PostalCode,
			Phone:      item.Phone,
			FAX:        item.FAX,
			Email:      item.Email,
		})
	}

	return res

}
