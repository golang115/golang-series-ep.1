package router

import (
	service "go-series/pkg/services"

	v1 "go-series/pkg/api/v1"

	"github.com/gofiber/fiber/v2"
)

func RegisterRoutes(app *fiber.App, s service.ICustomerService) {
	v1CustomerHandler := v1.NewCustomerHandler(s)
	v1CustomerHandlerRoutes := app.Group("v1/customer")
	v1CustomerHandlerRoutes.Get("/list", v1CustomerHandler.GetAll)
}
