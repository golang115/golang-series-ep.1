package model

type CustomerResponse struct {
	Customers []CustomerModel
}

type CustomerModel struct {
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	Company    string `json:"company"`
	Address    string `json:"address"`
	City       string `json:"city"`
	State      string `json:"state"`
	Country    string `json:"country"`
	PostalCode string `json:"postal_code"`
	Phone      string `json:"phone"`
	FAX        string `json:"fax"`
	Email      string `json:"email"`
}
