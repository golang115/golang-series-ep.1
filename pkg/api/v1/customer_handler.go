package v1

import (
	service "go-series/pkg/services"

	"github.com/gofiber/fiber/v2"
)

type CustomerHandler struct {
	service service.ICustomerService
}

func NewCustomerHandler(service service.ICustomerService) *CustomerHandler {
	return &CustomerHandler{service: service}
}

func (handler CustomerHandler) GetAll(c *fiber.Ctx) error {
	res := handler.service.GetAll()
	if res != nil {

	}
	return c.Status(fiber.StatusOK).JSON(res)
}
