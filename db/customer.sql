-- golang_db.customer definition

-- Drop table

-- DROP TABLE golang_db.customer;

CREATE TABLE golang_db.customer (
	customer_id int4 NOT NULL,
	first_name varchar(40) NOT NULL,
	last_name varchar(20) NOT NULL,
	company varchar(80) NULL,
	address varchar(70) NULL,
	city varchar(40) NULL,
	state varchar(40) NULL,
	country varchar(40) NULL,
	postal_code varchar(10) NULL,
	phone varchar(24) NULL,
	fax varchar(24) NULL,
	email varchar(60) NOT NULL,
	supportep_id int4 NULL
);





INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(1, 'Luís', 'Gonçalves', 'Embraer - Empresa Brasileira de Aeronáutica S.A.', 'Av. Brigadeiro Faria Lima, 2170', 'São José dos Campos', 'SP', 'Brazil', '12227-000', '+55 (12) 3923-5555', '+55 (12) 3923-5566', 'luisg@embraer.com.br', 3);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(2, 'Leonie', 'Köhler', NULL, 'Theodor-Heuss-Straße 34', 'Stuttgart', NULL, 'Germany', '70174', '+49 0711 2842222', NULL, 'leonekohler@surfeu.de', 5);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(3, 'François', 'Tremblay', NULL, '1498 rue Bélanger', 'Montréal', 'QC', 'Canada', 'H2G 1A7', '+1 (514) 721-4711', NULL, 'ftremblay@gmail.com', 3);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(4, 'Bjørn', 'Hansen', NULL, 'Ullevålsveien 14', 'Oslo', NULL, 'Norway', '0171', '+47 22 44 22 22', NULL, 'bjorn.hansen@yahoo.no', 4);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(5, 'František', 'Wichterlová', 'JetBrains s.r.o.', 'Klanova 9/506', 'Prague', NULL, 'Czech Republic', '14700', '+420 2 4172 5555', '+420 2 4172 5555', 'frantisekw@jetbrains.com', 4);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(6, 'Helena', 'Holý', NULL, 'Rilská 3174/6', 'Prague', NULL, 'Czech Republic', '14300', '+420 2 4177 0449', NULL, 'hholy@gmail.com', 5);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(7, 'Astrid', 'Gruber', NULL, 'Rotenturmstraße 4, 1010 Innere Stadt', 'Vienne', NULL, 'Austria', '1010', '+43 01 5134505', NULL, 'astrid.gruber@apple.at', 5);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(8, 'Daan', 'Peeters', NULL, 'Grétrystraat 63', 'Brussels', NULL, 'Belgium', '1000', '+32 02 219 03 03', NULL, 'daan_peeters@apple.be', 4);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(9, 'Kara', 'Nielsen', NULL, 'Sønder Boulevard 51', 'Copenhagen', NULL, 'Denmark', '1720', '+453 3331 9991', NULL, 'kara.nielsen@jubii.dk', 4);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(10, 'Eduardo', 'Martins', 'Woodstock Discos', 'Rua Dr. Falcão Filho, 155', 'São Paulo', 'SP', 'Brazil', '01007-010', '+55 (11) 3033-5446', '+55 (11) 3033-4564', 'eduardo@woodstock.com.br', 4);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(11, 'Alexandre', 'Rocha', 'Banco do Brasil S.A.', 'Av. Paulista, 2022', 'São Paulo', 'SP', 'Brazil', '01310-200', '+55 (11) 3055-3278', '+55 (11) 3055-8131', 'alero@uol.com.br', 5);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(12, 'Roberto', 'Almeida', 'Riotur', 'Praça Pio X, 119', 'Rio de Janeiro', 'RJ', 'Brazil', '20040-020', '+55 (21) 2271-7000', '+55 (21) 2271-7070', 'roberto.almeida@riotur.gov.br', 3);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(13, 'Fernanda', 'Ramos', NULL, 'Qe 7 Bloco G', 'Brasília', 'DF', 'Brazil', '71020-677', '+55 (61) 3363-5547', '+55 (61) 3363-7855', 'fernadaramos4@uol.com.br', 4);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(14, 'Mark', 'Philips', 'Telus', '8210 111 ST NW', 'Edmonton', 'AB', 'Canada', 'T6G 2C7', '+1 (780) 434-4554', '+1 (780) 434-5565', 'mphilips12@shaw.ca', 5);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(15, 'Jennifer', 'Peterson', 'Rogers Canada', '700 W Pender Street', 'Vancouver', 'BC', 'Canada', 'V6C 1G8', '+1 (604) 688-2255', '+1 (604) 688-8756', 'jenniferp@rogers.ca', 3);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(16, 'Frank', 'Harris', 'Google Inc.', '1600 Amphitheatre Parkway', 'Mountain View', 'CA', 'USA', '94043-1351', '+1 (650) 253-0000', '+1 (650) 253-0000', 'fharris@google.com', 4);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(17, 'Jack', 'Smith', 'Microsoft Corporation', '1 Microsoft Way', 'Redmond', 'WA', 'USA', '98052-8300', '+1 (425) 882-8080', '+1 (425) 882-8081', 'jacksmith@microsoft.com', 5);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(18, 'Michelle', 'Brooks', NULL, '627 Broadway', 'New York', 'NY', 'USA', '10012-2612', '+1 (212) 221-3546', '+1 (212) 221-4679', 'michelleb@aol.com', 3);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(19, 'Tim', 'Goyer', 'Apple Inc.', '1 Infinite Loop', 'Cupertino', 'CA', 'USA', '95014', '+1 (408) 996-1010', '+1 (408) 996-1011', 'tgoyer@apple.com', 3);
INSERT INTO customer
(customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email, supportep_id)
VALUES(20, 'Dan', 'Miller', NULL, '541 Del Medio Avenue', 'Mountain View', 'CA', 'USA', '94040-111', '+1 (650) 644-3358', NULL, 'dmiller@comcast.com', 4);
